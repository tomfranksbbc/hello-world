FROM node:6.0.0
WORKDIR /src
CMD npm i && npm start
EXPOSE 8080

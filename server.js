var express = require('express'),
    port = 8080,
    app = express(),
    pjson = require('./package.json');

app.get('/', function (req, res) {
  res.send('Hello world\n');
});

app.get('/version', function (req, res) {
    res.json({ version: pjson.version });
});

app.listen(port);
console.log('Running on http://0.0.0.0:' + port);
